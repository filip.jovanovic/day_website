import mysql.connector
import json
config = {
    'user':'root',
    'password': 'password',
    'host': 'localhost',
    'database': 'day_website'
}

db = mysql.connector.connect(**config)
cursor = db.cursor()


def get_posts():
    sql_command = f"SELECT url, tekst, naslov, kategorija FROM posts"
    cursor.execute(sql_command)

    posts = cursor.fetchall()
    result = []
    for post in posts:
        obj = {
            'url': post[0],
            'tekst': post[1],
            'naslov': post[2],
            'kategorija': post[3]
        }
        result.append(obj)
    return result

def send_email(name, email, subject, message):
    sql_command = f"INSERT INTO contact(name, email, subject, message) VALUES('{name}', '{email}', '{subject}', '{message}')"
    cursor.execute(sql_command)
    db.commit()


