from flask import Flask ,request
from flask_restful import Api,Resource
from flask_cors import CORS

from db import get_posts, send_email

app = Flask(__name__)
CORS(app)
api = Api(app)
app.config['CORS-HEADERS'] = 'Content-Type'


class Posts(Resource):
    def get(self):
        data = get_posts()
        return data, 200
    
    
class Contact(Resource):
    def post(self):
        content_type = request.headers.get('Content-Type')
        body = ''
        if (content_type == 'application/json'):
            body = request.json
        else:
            return 'Content-Type not supported!', 400

        try:
            send_email(body['name'], body['email'], body['subject'], body['message'])
            return 'Email saved in db', 200
        except Exception as err:
            return err, 400
        

    
api.add_resource(Posts, '/get_posts')
api.add_resource(Contact, '/send_email')


if __name__ == "__main__":
    app.run(debug=True)
