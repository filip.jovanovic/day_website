const filters = document.querySelectorAll('.filter-option');
const posts_grid = document.querySelector('.posts-grid');
const filter_options = document.querySelectorAll('.filter-option');

filters.forEach(filter => filter.addEventListener('click', posts_filter));

async function show_all_posts(){
    const response = await fetch('http://127.0.0.1:5000/get_posts');
    const data = await response.json();

    data.forEach(post => {
        const url = post.url;
        const tekst = post.tekst;
        const naslov = post.naslov;
        const kategorija = post.kategorija;

        posts_grid.innerHTML += `<img src=${url} class="post ${kategorija}">`
    });
};
show_all_posts();

async function posts_filter(e){
    const category = e.target.innerText;
    let posts = document.querySelectorAll('.post');
    const all_posts = document.querySelectorAll('.post');
    if(category != 'ALL'){
        posts = document.querySelectorAll(`.post.${category.toLowerCase()}`);
    }

    filter_options.forEach(option => option.style.color = 'black');
    e.target.style.color = 'red';

    all_posts.forEach(post => post.style.display = 'none');
    posts.forEach(post => post.style.display = 'block');
};


// ------------------------------------------------------------------------------------------------------------

const my_form = document.querySelector('#my-form');
// const submit_btn = document.querySelector('#submitBtn');
const name_field = document.querySelector('#name-input');
const email_field = document.querySelector('#email-input');
const subject_field = document.querySelector('#subject-input');
const message_field = document.querySelector('#message-input');
my_form.addEventListener('submit', submit_handler);

async function submit_handler(e){
    e.preventDefault();

    const data = {
        name: name_field.value,
        email: email_field.value,
        subject: subject_field.value,
        message: message_field.value
    };

    //make a post request
    const response = await fetch('http://127.0.0.1:5000/send_email', {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json"
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data)
    });

    alert("Email saved");
    name_field.value = '';
    email_field.value = '';
    subject_field.value = '';
    message_field.value = '';

    return response.json();
}


